Source: micromanager
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <mathieu.malaterre@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               javahelper,
               default-jdk,
               libtool,
               libgphoto2-2-dev,
               libboost-date-time-dev,
               libboost-thread-dev,
               libboost-system-dev,
               zlib1g-dev,
               swig,
               libdc1394-22-dev,
               libv4l-dev,
               libfreeimage-dev,
               libusb-dev
Build-Depends-Indep: libcommons-math-java,
                     imagej,
                     bsh,
                     libswingx-java,
                     libswing-layout-java,
                     clojure1.2,
                     clojure-contrib,
                     libwerken.xpath-java,
                     libjfreechart-java,
                     libnb-absolutelayout-java
Standards-Version: 4.1.1
Vcs-Browser: https://salsa.debian.org/med-team/micromanager
Vcs-Git: https://salsa.debian.org/med-team/micromanager.git
Homepage: https://micro-manager.org/

Package: micromanager-imagej
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ${java:Recommends}
Description: ImageJ User interface for Microscopy Software
 µManager is a software package for control of automated microscopes. It lets
 you execute common microscope image acquisition strategies such as time-lapses,
 multi-channel imaging, z-stacks, and combinations thereof. μManager works with
 microscopes from all four major manufacturers (Leica, Nikon, Olympus and
 Zeiss), most scientific-grade cameras and many peripherals (stages, filter
 wheels, shutters, etc.) used in microscope imaging (check the list of supported
 hardware). Since μManager runs as a plugin to ImageJ, image analysis routines
 are available within the application.
 .
 Unencumbered code provides a GUI for microscope image acquisition, a hardware
 interface layer and hardware interfacing for:
  - ASI stages, filter wheels and shutters
  - Arduino
  - Conix filter changer
  - Velleman K8055 and K8061 digital IO boards
  - Leica DMI microscopes
  - Ludl shutters, stages and filter Wheels
  - Nikon TE2000 microscope
  - Physik Instrumente stages
  - Pecon stage incubators
  - Prior shutters, stages and filter wheels
  - Spectral LMM5 laser controller
  - Sutter shutters, filter wheels and DG4
  - Vincent Uniblitz shutters
  - Yokogawa spinning disk confocal CSU22 and CSUX
  - Zeiss microscopes (two 'generations')
  - iidc1394 compatible cameras (through libdc1394)

Package: micromanager
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Microscopy Software
 µManager is a software package for control of automated microscopes. It lets
 you execute common microscope image acquisition strategies such as time-lapses,
 multi-channel imaging, z-stacks, and combinations thereof. μManager works with
 microscopes from all four major manufacturers (Leica, Nikon, Olympus and
 Zeiss), most scientific-grade cameras and many peripherals (stages, filter
 wheels, shutters, etc.) used in microscope imaging (check the list of supported
 hardware). Since μManager runs as a plugin to ImageJ, image analysis routines
 are available within the application.
 .
 Unencumbered code provides a GUI for microscope image acquisition, a hardware
 interface layer and hardware interfacing for:
  - ASI stages, filter wheels and shutters
  - Arduino
  - Conix filter changer
  - Velleman K8055 and K8061 digital IO boards
  - Leica DMI microscopes
  - Ludl shutters, stages and filter Wheels
  - Nikon TE2000 microscope
  - Physik Instrumente stages
  - Pecon stage incubators
  - Prior shutters, stages and filter wheels
  - Spectral LMM5 laser controller
  - Sutter shutters, filter wheels and DG4
  - Vincent Uniblitz shutters
  - Yokogawa spinning disk confocal CSU22 and CSUX
  - Zeiss microscopes (two 'generations')
  - iidc1394 compatible cameras (through libdc1394)
